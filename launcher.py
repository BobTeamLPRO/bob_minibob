import commands

def getIPAddress():
	return commands.getoutput('hostname -I').split()[0]

if __name__ == '__main__':
    commands.getoutput('nmap -sn ' + getIPAddress() + '/24')
    launchCommand = 'python audio.py '
    arp = commands.getoutput('arp -a')
    config = open('config.lock','r').read()
    for mac in config:
        if ':' in mac:
            for ip in arp:
                if mac in ip:
                    launchCommand = launchCommand + ' ' + ip.split('(')[1].split(')')[0]
    commands.getoutput(launchCommand)
