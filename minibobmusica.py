#!/usr/bin/env python
from socket import *
import subprocess
import fcntl
import struct
#import speech_api
import threading

IFNAME = "wlan0"

def get_ip_address():
	s = socket(AF_INET, SOCK_DGRAM)
	return inet_ntoa(fcntl.ioctl(
		s.fileno(),
		0x8915,  # SIOCGIFADDR
		struct.pack('256s', IFNAME[:15])
	)[20:24])

def hiloServer(connection,client_address):
	audio = ""
	try:
		f = open('archivo.mp3','wb')
		while True:
			connection.settimeout(10)
			data = connection.recv(1024)
			#if data:
			f.write(data)
			#else:
			#	f.close()
			#	subprocess.call('mplayer archivo.mp3', shell=True)

	except timeout:
		f.close()
		connection.close()
		subprocess.call('mplayer archivo.mp3', shell=True)

def main():
	host=get_ip_address()
    #host = "127.0.0.1"
	port = 9997
	s = socket(AF_INET,SOCK_STREAM)
	s.bind((host,port))
	s.listen(1)
   	addr = (host,port)
   	print("server start o IP: " + host + " PORT: " + str(port))
   	while True:
	    buf=1024

	    audio=""

	    connection, client_address = s.accept()

	    t = threading.Thread(target=hiloServer,args=(connection,client_address))
	    t.start()

if __name__ == "__main__":
    main()
