from socket import *
import sys
import subprocess

if len(sys.argv) != 4:
	print "usage: python client.py [IP_DEST] [PORT_DEST] [FILENAME]"

s = socket(AF_INET,SOCK_DGRAM)
host =sys.argv[1]
port = int(float(sys.argv[2]))
buf =1024
addr = (host,port)

file_name=sys.argv[3]

f=open(file_name,"rb") 
data = f.read(buf)

s.sendto(file_name,addr)
s.sendto(data,addr)
while (data):
    if(s.sendto(data,addr)):
        #print "sending ..."
        data = f.read(buf)
try:
	s.settimeout(10)
	data, server = s.recvfrom(4096)
except timeout:
	data = "la conexion con Bob esta tardando mas de lo normal"
	print "timeout"
s.close()
f.close()

print data
subprocess.call('espeak -v mb-es2 -a 400 -s 120 -p 80 "' + data + '"', shell=True)

#subprocess.call("rm " + file_name, shell=True)