#!/usr/bin/python
#Instead of adding silence at start and end of recording (values=0) I add the original audio . This makes audio sound more natural as volume is >0. See trim()
#I also fixed issue with the previous code - accumulated silence counter needs to be cleared once recording is resumed.

from array import array
#from struct import pack
from sys import byteorder
import copy
import pyaudio
#import wave
import os
import subprocess
import sys
import threading
import time
from socket import *
from struct import pack
import wave

THRESHOLD = 500  # audio levels not normalised.
CHUNK_SIZE = 4096
SILENT_CHUNKS = 0.75 *  16000 / 4096  # about 1sec
FORMAT = pyaudio.paInt16
FRAME_MAX_VALUE = 2 ** 15 - 1
NORMALIZE_MINUS_ONE_dB = 10 ** (-1.0 / 20)
RATE = 16000
CHANNELS = 1
TRIM_APPEND = RATE / 4
IP = ""
PORT = 9998
FIRST = True

MODONOCHE = False

stream = None

def hiloRuido():
    global stream
    # p = pyaudio.PyAudio()
    # stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, output=True, frames_per_buffer=CHUNK_SIZE)
    print "INICIALIZANDO HILO DE RUIDO"
    while True:
        time.sleep(20)
        print "ACTUALIZANDO HILO DE RUIDO "
        aux = True
        for x in xrange(1,8): #16 chunks = 1 seg
            data_chunk = array('h', stream.read(CHUNK_SIZE))
            actualize_noise(data_chunk,aux)
            aux =False

def actualize_noise(data_chunk,aux):
    global THRESHOLD
    global MODONOCHE
    #print "ruido actual: " + str(THRESHOLD)
    if aux:
        THRESHOLD = max(data_chunk)
    else:
        if max(data_chunk) > THRESHOLD:
            THRESHOLD = max(data_chunk)

    #if MODONOCHE:
    THRESHOLD = THRESHOLD + (THRESHOLD*0.1)
    #else:
    #    THRESHOLD = THRESHOLD + (THRESHOLD*0.4)

    #print "nuevo ruido: " + str(THRESHOLD)
    return True

def detect_noise(data_chunk):
    "Detect de base noise in the system"
    global THRESHOLD
    if max(data_chunk) > THRESHOLD:
        THRESHOLD = max(data_chunk)
    return True

def is_silent(data_chunk):
    """Returns 'True' if below the 'silent' THRESHOLD"""
    return max(data_chunk) < THRESHOLD

def normalize(data_all):
    """Amplify the volume out to max -1dB"""
    # MAXIMUM = 16384
    normalize_factor = (float(NORMALIZE_MINUS_ONE_dB * FRAME_MAX_VALUE)
                        / max(abs(i) for i in data_all))

    r = array('h')
    for i in data_all:
        r.append(int(i * normalize_factor))
    return r

def trim(data_all):
    _from = 0
    _to = len(data_all) - 1
    for i, b in enumerate(data_all):
        if abs(b) > THRESHOLD:
            _from = max(0, i - TRIM_APPEND)
            break

    for i, b in enumerate(reversed(data_all)):
        if abs(b) > THRESHOLD:
            _to = min(len(data_all) - 1, len(data_all) - 1 - i + TRIM_APPEND)
            break

    return copy.deepcopy(data_all[_from:(_to + 1)])

def record():
    """Record a word or words from the microphone and 
    return the data as an array of signed shorts."""
    global stream

    s = socket(AF_INET,SOCK_STREAM)
    addr = (IP,PORT)

    p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, output=True, frames_per_buffer=CHUNK_SIZE)

    silent_chunks = 0
    audio_started = False
    data_all = array('h')

    global FIRST
    if FIRST:
        for x in xrange(1,3): #16 chunks = 1 seg
            data_chunk = array('h', stream.read(CHUNK_SIZE))
            detect_noise(data_chunk)
        FIRST = False

    while True:
        # little endian, signed short
        data_chunk = array('h', stream.read(CHUNK_SIZE))
        if byteorder == 'big':
            data_chunk.byteswap()

        silent = is_silent(data_chunk)

        if audio_started:
            if silent:
                silent_chunks += 1
                if silent_chunks > SILENT_CHUNKS:
                    break
                data_all.extend(data_chunk)
                s.sendall(data_chunk)
            else: 
                data_all.extend(data_chunk)
                s.sendall(data_chunk)
                silent_chunks = 0
                pass
        else:
            if not silent:
                audio_started = True
                data_all.extend(data_chunk)
                s.connect(addr)
                s.sendall(data_chunk)
                print("recording");
                pass
            else:
                data_all = array('h')  
                data_all.extend(data_chunk)  
                pass      

    respu = s.recv(2048)

    global MODONOCHE
    if respu == "modo noche activado":
        MODONOCHE = True
    print respu
    subprocess.call('espeak -v mb-es2 -a 400 -s 120 -p 80 "' + respu + '"', shell=True)
    s.close()

    sample_width = p.get_sample_size(FORMAT)
    stream.stop_stream()
    stream.close()
    p.terminate()


    #data_all = trim(data_all)  # we trim before normalize as threshhold applies to un-normalized wave (as well as is_silent() function)

    #data_all = normalize(data_all)

    return #sample_width,data_all

# def record_to_file(path):
#     "Records from the microphone and outputs the resulting data to 'path'"
#     sample_width, data = record()
#     data = pack('<' + ('h' * len(data)), *data)

#     # print sample_width

#     #Se guarda el audio en un archivo wav
#     wave_file = wave.open(path, 'wb')
#     wave_file.setnchannels(CHANNELS)
#     wave_file.setsampwidth(sample_width)
#     wave_file.setframerate(RATE)
#     wave_file.writeframes(data)
#     wave_file.close()
#     # #Se convierte el archivo wav en uno flac (para GSA)
#     # subprocess.call("flac -f " + path, shell=True)
#     # #Se elimina el archivo wav
#     # subprocess.call("rm " + path, shell=True)
#     # new_name = path.replace('.wav','.flac')
#     #orden = "python client.py " + IP + " " + PORT +" " + path
#     #subprocess.call(orden, shell=True)
#     #subprocess.call("rm " + new_name, shell=True)
#     s = socket()
#     s.connect((IP, PORT))

#     f = open("prueba.wav", "rb")
#     content = f.read(1024)     
#     while content:
#         # Enviar contenido.
#         s.sendall(content)
#         content = f.read(1024)

#     respu = s.recv(2048)

#     global MODONOCHE
#     if respu == "modo noche activado":
#         MODONOCHE = True
#     print respu
#     subprocess.call('espeak -v mb-es2 -a 400 -s 120 -p 80 "' + respu + '"', shell=True)
#     s.close()


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "usage: python audio.py [IP_DEST] [PORT_DEST]"
        sys.exit(0)
    subprocess.call("./minibobserver.py", shell=True)
    subprocess.call("./minibobmusica.py", shell=True)
    t = threading.Thread(target=hiloRuido)
    t.start()
    # global IP
    # global PORT
    IP = sys.argv[1]
    PORT = int(float(sys.argv[2]))
    i=0
    while True:
        print("Wait in silence to begin recording; wait in silence to terminate")
        record()
        #record_to_file('prueba.wav')
        i+=1
