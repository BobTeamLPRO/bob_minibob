#!/usr/bin/env python
from socket import *
import fcntl
import struct
import threading
import subprocess

IFNAME = "wlan0"

def get_ip_address():
	s = socket(AF_INET, SOCK_DGRAM)
	return inet_ntoa(fcntl.ioctl(
		s.fileno(),
		0x8915,  # SIOCGIFADDR
		struct.pack('256s', IFNAME[:15])
	)[20:24])

def hiloServer(connection,client_address):
	audio = ""
	try:
		while True:
			connection.settimeout(1)
			data = connection.recv(1024)
			if data:
				audio = audio + str(data)
			else:
				break
	except timeout:
		pass
	finally:
		connection.close()
		subprocess.call('espeak -v mb-es2 -a 400 -s 120 -p 80 "' + audio + '"', shell=True)

def main():
	host=get_ip_address()
    #host = "127.0.0.1"
	port = 9998
	s = socket(AF_INET,SOCK_STREAM)
	s.bind((host,port))
	s.listen(1)
   	addr = (host,port)
   	print("server start o IP: " + host + " PORT: " + str(port))
   	while True:
	    buf=1024

	    audio=""

	    connection, client_address = s.accept()

	    t = threading.Thread(target=hiloServer,args=(connection,client_address))
	    t.start()

if __name__ == "__main__":
    main()
