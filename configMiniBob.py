import sys
import commands
import time
import socket

ssid = 'BOBLPRO20'
password = '2016LPROBOB'
macBob = ''

def getIPAddress():
	return commands.getoutput('hostname -I').split()[0]

def launchServer():
    print 'launchServer()'
    commands.getoutput('nmap -sn ' + getIPAddress() + '/24')
    ip = commands.getoutput('arp -a  | grep ' + macBob)
    launchCommand = 'python audio.py ' + ip.split('(')[1]
    commands.getoutput(launchCommand)

def configLock():
    print 'configLock()'
    config = open("config.lock","w")
    config.write(ssid + '\n')
    config.write(password + '\n')
    config.write(macBob)
    config.close()

def newMiniBob(mac):
	global ssid
	global password

	while True:
		if (commands.getstatusoutput('ping -c 3 10.0.0.1')[0] == 0):
			print 'newMiniBob() -> ' + mac

			#ip = commands.getoutput('ip addr show wlan0 | grep inet').split()
			s = socket.socket()
			s.connect(('10.0.0.1', 9990))
			s.send('newMiniBob*' + mac)
			print 'esperando a bob por ssid y pass'
			data = s.recv(1024).split('*')
			print data
			print 'miniBob ' + mac + ' -> ' + data[2] + ' ' + data[3]
			ssid = data[2]
			password = data[3]
			c.close()
			break

def configWifi():
    print 'configWifi()'
    wpa = open("interfaces","w")

    wpa.write("source-directory /etc/network/interfaces.d \n")

    wpa.write("auto lo \n")
    wpa.write("iface lo inet loopback \n")
    wpa.write("iface eth0 inet manual \n")
    wpa.write("allow-hotplug wlan0 \n")
    wpa.write("auto wlan0 \n")
    wpa.write("iface wlan0 inet dhcp \n")
    wpa.write("wpa-ssid \""+ ssid +"\" \n")
    wpa.write("wpa-psk \""+ password +"\" \n")

    wpa.close()

    commands.getoutput('sudo ifdown wlan0')
    commands.getoutput('sudo cp /home/pi/miniBOB/interfaces /etc/network/interfaces')
    commands.getoutput('sudo ifup wlan0')

def getMAC(interface):
    print 'getMac()'
    try:
        str = open('/sys/class/net/' + interface + '/address').read()
    except:
        str = "00:00:00:00:00:00"
    return str[0:17]


if __name__ == '__main__':

	mac = getMAC("wlan0")
	configWifi()
	time.sleep(10)
	newMiniBob(mac)
	time.sleep(10)
	configWifi()
	time.sleep(10)
	if (commands.getstatusoutput('ping -c 10 8.8.8.8')[0] == 0):
		print 'Pimera entrada'
		configLock()
		launchServer()
	else:
		time.sleep(10)
		print 'Control'
		if (commands.getstatusoutput('ping -c 10 8.8.8.8')[0] == 0):
			print 'Segunda entrada'
			configLock()
			launchServer()
		else:
			print 'sudo reboot'
			#sudo reboot
